import 'dart:async';
import 'package:sensors/sensors.dart';
import 'package:rxdart/rxdart.dart';

class BrujulaBloc {
  List<double> _tilt;

  //
  // Stream to handle the compass
  //
  StreamController<List<double>> _compassController = StreamController<List<double>>();
  StreamSink<List<double>> get _inAdd => _compassController.sink;
  Stream<List<double>> get outTilt => _compassController.stream;

  //
  // Stream to handle the action on the counter
  //
  StreamController _actionController = StreamController();
  StreamSink get calculateTilt => _actionController.sink;

  //
  // Constructor
  //
  CalculateTilt(){
    _tilt = tilt();
    _actionController.stream
        .listen(_handleLogic);
  }

  void dispose(){
    _actionController.close();
    _compassController.close();
  }

  void _handleLogic(data){
    _tilt = tilt();
    _inAdd.add(_tilt);
  }

  List<double> tilt() {
    gyroscopeEvents.listen((GyroscopeEvent event) {
      var cancer = <double>[event.x, event.y, event.z];
      print(cancer.first);
    });
  }
}