import 'package:flutter/material.dart';
//import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:sensors/sensors.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Herramientas y sensores',
      theme: new ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(

      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;
  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _angle;
  double _northAngle;
  List<double> _things;
  int BNindex = 0;
  List<StreamSubscription<dynamic>> _streamSubscriptions =
  <StreamSubscription<dynamic>>[];

  @override
  Widget build(BuildContext context) {
    final double angle = _angle;
    final List<double> things = _things;
    final double northAngle = _northAngle;

    return new Scaffold(

      appBar: null,

      body: new Center(
        child: tool(BNindex, angle, northAngle, things),
      ),

      bottomNavigationBar: BottomNavigationBar(
        onTap: (it) {
          setState(() {
            BNindex = it;
          });

          //debugPrint("index: $BNindex");
        },
        currentIndex: BNindex,
        items: [
          BottomNavigationBarItem(
            title: Text("Nivel"),
            icon: Icon(Icons.timeline),
          ),
          BottomNavigationBarItem(
            title: Text("Brujula"),
            icon: Icon(Icons.timer),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _streamSubscriptions.add(
        accelerometerEvents.listen((AccelerometerEvent event) {
          setState(() {
            var angulo = event.y* 9.174312;//(gravedad-contra) * 9.174312;
            _things = [event.x, event.y, event.z];
            if(angulo >90.0)
              angulo = 90.0;
            _angle = angulo;
            _northAngle = angulo;
          });
        })
    );


    @override
    void dispose() {
      super.dispose();
      for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
        subscription.cancel();
      }
    }
  }

}

Widget tool(int index, double angle, double northAngle, List<double> things) {
  switch(index){
    case 0: return Level(angle);
    case 1: return Compass(northAngle, things);
  }
}

class Level extends StatelessWidget {

  double angle;

  Level(this.angle);

  @override
  Widget build(BuildContext context) {

    return Column(children:<Widget>[

      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Transform.rotate(
                angle: double.parse(angle.toStringAsFixed(1)) * (3.14 / 180),
                child: Container(
                  color: Colors.blue,
                  height: 3.0,
                  width: 1000.0,
                ),
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Text(
          angle.toStringAsFixed(1) + "º",
          style: Theme
              .of(context)
              .textTheme
              .display1,
        ),
      ),
    ]);
  }
}

class Compass extends StatelessWidget{

  double northAngle;
  List<double> things;

  Compass(this.northAngle, this.things);

  @override
  Widget build(BuildContext context){
    return Column(children:<Widget>[

      Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Text(
          "N",
          style: Theme
              .of(context)
              .textTheme
              .display1,
        ),
      ),

      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Transform.rotate(
                angle: northAngle * (3.14 / 180),
                child: Container(
                  color: Colors.blue,
                  height: 3.0,
                  width: 1000.0,
                ),
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Text(
          things.toString(),
          style: Theme
              .of(context)
              .textTheme
              .display1,
        ),
      ),
    ]);
  }

  @override
  State<StatefulWidget> createState() {

  }

}

